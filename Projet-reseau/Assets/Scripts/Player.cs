using Unity.Netcode;
using UnityEngine;
using System.Collections;
using TMPro;

public class Player : NetworkBehaviour
{
    [SerializeField]
    float moveSpeed = 70f;

    //public ulong playerID;
    public NetworkVariable<int> playerID = new NetworkVariable<int>();

    [SerializeField]
    private TextMeshProUGUI playerIDText;
    [SerializeField]
    private TextMeshProUGUI playerScoreText;

    [SerializeField]
    //private GameObject multiplicatorIndicator;

    //public NetworkVariable<bool> showMultiplierIndicator = new NetworkVariable<bool>(false);

    Vector3 forward, right;
    Rigidbody rb;

    public NetworkVariable<Vector3> forwardNV = new NetworkVariable<Vector3>();
    public NetworkVariable<Vector3> rightNV = new NetworkVariable<Vector3>();
    public NetworkVariable<Vector3> upNV = new NetworkVariable<Vector3>();

    //Shader
    private int _id;
    public string shaderVariableName = "_DistanceToCamera";
    private Vector3 _distanceObjectToCamera;


    void Start()
    {
        forward = Camera.main.transform.forward;
        forward.y = 0;
        forward = Vector3.Normalize(forward);
        right = Quaternion.Euler(new Vector3(0, 90, 0)) * forward;
        rb = GetComponent<Rigidbody>();
        if (IsServer)
            playerID.Value = GameManager.Instance.GetID();
    }

    void Update()
    {
        if(IsServer)
            UpdateServer();
        if(IsClient)
            UpdateClient();
    }

    private void UpdateServer()
    {
    if (playerIDText && !IsOwner)
        playerIDText.text = "Player " + playerID.Value;

    if (playerScoreText)
        playerScoreText.text = "Score: " + GameManager.Instance.GetScore(playerID.Value);
    }

    private void UpdateClient()
    {
        if(IsOwner)
            MoveInput();

        if(playerIDText && !IsOwner)
            playerIDText.text = "Player " + playerID.Value;
        else if(playerIDText)
            playerIDText.text = "You";

        if(playerScoreText)
            playerScoreText.text = "Score: " + GameManager.Instance.GetScore(playerID.Value);

        /*if(showMultiplierIndicator.Value)
        {
            multiplicatorIndicator.SetActive(true);
        }
        else
        {
            multiplicatorIndicator.SetActive(false);
        }*/

    }
    
    void MoveInput()
    {
        Vector3 direction = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        Vector3 rightMovement = right * moveSpeed * Time.deltaTime * Input.GetAxis("Horizontal");
        Vector3 upMovement = forward * moveSpeed * Time.deltaTime * Input.GetAxis("Vertical");

        Vector3 heading = rightMovement + upMovement;

        //Mathf.LerpAngle(,,Time.time);

        ClientMoveAndRotateServerRpc(rightMovement, upMovement, heading);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(IsServer && other.gameObject.tag == "Coin")
        {
            GameManager.Instance.AddScore(playerID.Value, other.gameObject.GetComponent<Coin>().getValue());
            other.gameObject.SetActive(false);
            Destroy(other.gameObject);
        }
        else if (IsServer && other.gameObject.tag == "PowerUp")
        {
            other.gameObject.GetComponent<PowerUp>().Activate(this);
            Destroy(other.gameObject);
        }
    }

    public void BoostSpeed()
    {
        StartCoroutine(BoostSpeedCoroutine());
    }

    private IEnumerator BoostSpeedCoroutine()
    {
        moveSpeed *= 1.5f;
        yield return new WaitForSeconds(5);
        moveSpeed /= 1.5f;
    }

    public void BoostMultiplier()
    {
        StartCoroutine(GameManager.Instance.BoostMultiplierCoroutine());
    }

    [ServerRpc]
    public void ClientMoveAndRotateServerRpc(Vector3 right, Vector3 up, Vector3 heading)
    {
        transform.forward = heading; // TODO : remplacer par modification de la rotation Y
        Vector3 mv = (right + up);
        if (mv.sqrMagnitude > 1)
        {
            mv.Normalize();

        }
        rb.AddForce(mv * moveSpeed);
        rb.velocity = new Vector3(Mathf.Clamp(rb.velocity.x, -moveSpeed, moveSpeed), Mathf.Clamp(rb.velocity.y, -moveSpeed, moveSpeed), Mathf.Clamp(rb.velocity.z, -moveSpeed, moveSpeed));
    }
}
