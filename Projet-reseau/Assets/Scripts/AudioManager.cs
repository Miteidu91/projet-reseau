using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }        
    }
    
    public void Play(AudioSource audioSource)
    {
        audioSource.Play();
    }

    public void PlayAt(AudioClip[] audioClips, AudioSource audioSource)
    {
        if (!audioSource.isPlaying)
        {
            playRandom(audioClips, audioSource);
        }
    }


    public void playRandom(AudioClip[] audioClips, AudioSource audioSource)
    {
        Random.InitState(System.DateTime.Now.Millisecond);
        int r = Random.Range(0,audioClips.Length);
        audioSource.clip = audioClips[r];
        audioSource.Play();
    }
}
