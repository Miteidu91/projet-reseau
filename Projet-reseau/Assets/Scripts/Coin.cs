using System.Collections;
using UnityEngine;
using Unity.Netcode;

public class Coin : NetworkBehaviour
{
    private static int[] values = {7, 9, 12, 15};
    private int value;

    [SerializeField] private float desiredFallingDuration = 3f;
    private Vector3 startPosition;
    private Vector3 endPosition;

    public int getValue() {
        return value;
    }

    public int[] getValues() {
        return values;
    }

    private void Start() {
        System.Random rd = new System.Random();
        int rdVal = rd.Next(0,3);
        value = getValues()[rdVal];

        /*startPosition = this.transform.position;
        endPosition = new Vector3(this.transform.position.x, this.transform.position.y - 150f, this.transform.position.z);
        StartCoroutine(FallCorou());*/
    }

    IEnumerator FallCorou()
    {
        float elapsedTime = 0;

        while (elapsedTime < desiredFallingDuration)
        {
            this.transform.position = Vector3.Lerp(startPosition, endPosition, elapsedTime / desiredFallingDuration);
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        elapsedTime = 0;
        this.transform.position = endPosition;
    }

}
