using Unity.Netcode;
using UnityEngine;

public class ObjectSpawner : NetworkBehaviour
{
    public static ObjectSpawner Instance;

    [SerializeField] private GameObject[] powerUps;
    [SerializeField] private float powerUpSpawnRate = 8f;
    private float powerUpSpawnTimer;

    [SerializeField] private GameObject coin;
    [SerializeField] private float coinSpawnRate = 2f;
    private float coinSpawnTimer;

    private bool spawnActive = false;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Start()
    {
        powerUpSpawnTimer = powerUpSpawnRate;
        coinSpawnTimer = coinSpawnRate;
    }
    void Update()
    {
        if (IsServer && spawnActive)
            UpdateServer();
    }

    public void SetSpawnActive(bool active)
    {
        spawnActive = active;
    }

    private void UpdateServer()
    {
        if(powerUpSpawnTimer >= powerUpSpawnRate)
        {
            SpawnPowerUp();
            powerUpSpawnTimer = 0f;
        }
        powerUpSpawnTimer += Time.deltaTime;

        if (coinSpawnTimer >= coinSpawnRate)
        {
            SpawnCoin();
            coinSpawnTimer = 0f;
        }
        coinSpawnTimer += Time.deltaTime;
    }
    private Vector2 getRandomPos()
    {
        return new Vector2(Random.Range(-18f, 78f), Random.Range(-18f, 78f));
    }

    private void SpawnPowerUp()
    {
        Vector2 Position = getRandomPos();

        GameObject powerUpToSpawn = Instantiate(powerUps[Random.Range(0, powerUps.Length)], new Vector3(Position.x, 150f, Position.y), Quaternion.identity);
        powerUpToSpawn.GetComponent<NetworkObject>().Spawn();
    }

    private void SpawnCoin()
    {
        Vector2 Position = getRandomPos();

        GameObject coinToSpawn = Instantiate(coin, new Vector3(Position.x, 150f, Position.y), Quaternion.identity);
        coinToSpawn.GetComponent<NetworkObject>().Spawn();
    }

}
