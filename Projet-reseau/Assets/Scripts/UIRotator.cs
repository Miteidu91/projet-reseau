using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIRotator : MonoBehaviour
{
    private Camera cameraM;

    void Start()
    {
        cameraM = Camera.main;
        gameObject.GetComponent<Canvas>().worldCamera = cameraM;
    }
    void Update()
    {
        transform.forward = cameraM.transform.forward;
        transform.LookAt(transform.position + cameraM.transform.rotation * Vector3.forward, cameraM.transform.rotation * Vector3.up);
    }
}
