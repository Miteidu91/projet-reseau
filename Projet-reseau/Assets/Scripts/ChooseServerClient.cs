using Unity.Netcode;
using UnityEngine;

public class ChooseServerClient : MonoBehaviour
{
     [SerializeField]
     private GameObject UIMenu;
    void OnGUI()
    {
        /*GUILayout.BeginArea(new Rect(10, 10, 300, 300));
        if (!NetworkManager.Singleton.IsClient && !NetworkManager.Singleton.IsServer)
        {
            StartButtons();
        }
        else
        {
            StatusLabels();
        }

        GUILayout.EndArea();*/
    }

    public void ShowMenu()
    {
        UIMenu.SetActive(true);
    }

    public void HideMenu()
    {
        UIMenu.SetActive(false);
    }

    public void StartServer()
    {
        NetworkManager.Singleton.StartServer();
        HideMenu();
    }

    public void StartClient()
    {
        NetworkManager.Singleton.StartClient();
        HideMenu();
    }

    public void Quit()
    {
        Application.Quit();
    }
    static void StartButtons()
    {
        if (GUILayout.Button("Client"))
        {
            NetworkManager.Singleton.StartClient();
        }
        if (GUILayout.Button("Server"))
        {
            NetworkManager.Singleton.StartServer();
        }
        /*if (GUILayout.Button("Host"))
        {
            NetworkManager.Singleton.StartHost();
        }*/

    }

    static void StatusLabels()
    {
        var mode = NetworkManager.Singleton.IsServer ? "Server" : "Client";

        GUILayout.Label("Mode: " + mode);
    }

}