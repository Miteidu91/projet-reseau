using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeeThroughWalls : MonoBehaviour
{
    public LayerMask rayLayerMask;
    void Update()
    {
        RaycastHit hit;

        // Does the ray intersects with the sphere ?
        if(Physics.Raycast(Camera.main.transform.position,
            (this.transform.position - Camera.main.transform.position).normalized,
            out hit, Mathf.Infinity, rayLayerMask))
        {
            //if it collides with the sphere, scale to 0
            if(hit.collider.gameObject.tag == "SphereMask")
            {
                this.transform.localScale = new Vector3(0f, 0f, 0f);
            }
            //if does not collide, scale it to 6*
            else
            {
                this.transform.localScale = new Vector3(5f, 5f, 5f);
            }
        }
    }
}
