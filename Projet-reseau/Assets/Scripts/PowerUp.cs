using System.Collections;
using UnityEngine;
using Unity.Netcode;

public class PowerUp : NetworkBehaviour
{
    [SerializeField] private float desiredFallingDuration = 6f;
    private Vector3 startPosition;
    private Vector3 endPosition;

    public enum PowerUpType
    {
        Speed,
        Multiplier
    }

    private void Start()
    {
        /*startPosition = this.transform.position;
        endPosition = new Vector3(this.transform.position.x, this.transform.position.y - 150f, this.transform.position.z);
        StartCoroutine(FallCorou());*/
    }

    [SerializeField] private PowerUpType type = PowerUpType.Speed;
    public void Activate(Player plc)
    {
        switch (type)
        {
            case PowerUpType.Speed:
                plc.BoostSpeed();
                break;
            case PowerUpType.Multiplier:
                plc.BoostMultiplier();
                break;
        }
    }

    IEnumerator FallCorou()
    {
        float elapsedTime = 0;

        while (elapsedTime < desiredFallingDuration)
        {
            this.transform.position = Vector3.Lerp(startPosition, endPosition, elapsedTime / desiredFallingDuration);
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        elapsedTime = 0;
        this.transform.position = endPosition;
    }
}
