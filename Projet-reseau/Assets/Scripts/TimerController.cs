using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;
using UnityEngine.UI;
using TMPro;

public class TimerController : NetworkBehaviour
{
    public static TimerController instance;
    [SerializeField]
    private TextMeshProUGUI timeCounter;
    private TimeSpan timePlaying;

    [SerializeField]
    private float initialTime = 60.0f;
    private NetworkVariable<float> remainingTime = new NetworkVariable<float>();

    private void Awake() {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start() {
        timeCounter.text = "Duration of a game: "+initialTime.ToString();
    }

    public void ResetTimer()
    {
        remainingTime.Value = -1.0f;
        remainingTime.Value = initialTime;
    }

    public void UpdateClientTimer()
    {
        timePlaying = TimeSpan.FromSeconds(remainingTime.Value);
        timeCounter.text = "Remaining time: " + timePlaying.ToString("mm':'ss'.'f");
    }

    public void StartTimer() {
        remainingTime.Value = initialTime;
        StartCoroutine(UpdateTimer());
    }

    public void EndTimer() {
        timeCounter.text = "End of round!";
        GameManager.Instance.EndOfRound();
    }

    IEnumerator UpdateTimer() {
        while (remainingTime.Value > 0 && GameManager.Instance.timerStarted.Value) {
            remainingTime.Value -= Time.deltaTime;
            timePlaying = TimeSpan.FromSeconds(remainingTime.Value);
            timeCounter.text = "Remaining time: " + timePlaying.ToString("mm':'ss'.'f");
            yield return null;        
        }
        EndTimer();
    }

}
