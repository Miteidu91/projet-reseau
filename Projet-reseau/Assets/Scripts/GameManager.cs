using System.Collections;
using Unity.Netcode;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : NetworkBehaviour
{
    public static GameManager Instance;

    public AudioManager audioManager = null;

    public GameObject winCanvas;
    public TextMeshProUGUI winnerText;
    public TextMeshProUGUI score1Text;
    public TextMeshProUGUI score2Text;

    private bool wantToPasserCredits = false;
    private bool inCredits = false;

    private int totalNumberLevel = 1;

    public NetworkVariable<bool> timerStarted = new NetworkVariable<bool>();
    public float localScoreMultiplier = 1.0f;

    public NetworkVariable<float> remotePlayer1Score = new NetworkVariable<float>();
    public NetworkVariable<float> remotePlayer2Score = new NetworkVariable<float>();
    
    private NetworkVariable<int> maxID = new NetworkVariable<int>(0);
    private NetworkVariable<int> winnerID = new NetworkVariable<int>(-1);


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        if (NetworkManager.Singleton.IsServer)
            timerStarted.Value = false;
    }

    private void Start()
    {
        audioManager = AudioManager.Instance;
        Physics.gravity = new Vector3(0, -50.0f, 0);
        //TimerController.instance.StartTimer();
    }

    private void Update()
    {

        if(inCredits && wantToPasserCredits)
        {
            wantToPasserCredits = false;
            inCredits = false;
            GameManager.Instance.Menu();
        } 
        wantToPasserCredits = false;
        if (NetworkManager.Singleton.IsServer)
            ServerUpdate();
        else
            ClientUpdate();
    }

    private void ServerUpdate()
    {
        if (!timerStarted.Value && NetworkManager.Singleton.ConnectedClients.Count == 2 && winnerID.Value == -1)
        {
            remotePlayer2Score.Value = 0.0f;
            remotePlayer1Score.Value = 0.0f;
            timerStarted.Value = true;
            TimerController.instance.StartTimer();
            ObjectSpawner.Instance.SetSpawnActive(true);
        }
    }

    private void ClientUpdate()
    {

        if (timerStarted.Value == true)
        {
            TimerController.instance.UpdateClientTimer();
        }
        if(winnerID.Value != -1)
        {
            winCanvas.SetActive(true);
            if(winnerID.Value == 1)
            {
                winnerText.text = "Player 1 Wins!";
            }
            else if(winnerID.Value == 2)
            {
                winnerText.text = "Player 2 Wins!";
            }
            else
            {
                winnerText.text = "Draw!";
            }
            score1Text.text = "Score Player 1: " + remotePlayer1Score.Value.ToString();
            score2Text.text = "Score Player 2: " + remotePlayer2Score.Value.ToString();
        }
    }

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnMainSceneLoaded;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnMainSceneLoaded;
    }

    void OnMainSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == "EndGame")
            return;

        if (scene.name != "Menu" && scene.name != "Credits")
        {
            wantToPasserCredits = false;
            inCredits = false;
    
        } else if(scene.name == "Credits")
        {
            inCredits = true;
        }

    }

    public void LaunchGame()
    {
        LaunchLevel();

    }

    public void LaunchLevel()
    {
        SceneManager.LoadScene(1);
    }

    public void Menu()
    {
        SceneManager.LoadScene(0);
    }

    public void Credits()
    {
        SceneManager.LoadScene(totalNumberLevel + 1);
    }
    public void WantPasserCredits()
    {
        wantToPasserCredits = true;
    }

    public void AddScore(int playerID, float score)
    {

        if (timerStarted.Value && playerID == 1)
        {
            remotePlayer1Score.Value += score * localScoreMultiplier;
        }
        else if (timerStarted.Value && playerID == 2)
        {
            remotePlayer2Score.Value += score * localScoreMultiplier;
        }
    }


    public IEnumerator BoostMultiplierCoroutine()
    {
        localScoreMultiplier = 2.0f;
        yield return new WaitForSeconds(5);
        localScoreMultiplier = 1.0f;
    }

    public float GetScore(int playerID)
    {
        if (playerID == 1)
        {
            return remotePlayer1Score.Value;
        }
        else if (playerID == 2)
        {
            return remotePlayer2Score.Value;
        }
        return 12.0f;
    }

    public int GetID()
    {
        maxID.Value++;
        return maxID.Value;
    }

    public void EndOfRound()
    {
        if (NetworkManager.Singleton.IsServer)
        {
            ObjectSpawner.Instance.SetSpawnActive(false);
            //SceneManager.LoadScene(totalNumberLevel + 1);
            Debug.LogError(remotePlayer1Score.Value+"|"+remotePlayer2Score.Value);
            if (remotePlayer1Score.Value > remotePlayer2Score.Value)
            {
                winnerID.Value = 1;
                Debug.LogError("Player 1 win");
            }
            else if (remotePlayer2Score.Value > remotePlayer1Score.Value)
            {
                winnerID.Value = 2;
                Debug.LogError("Player 2 win");
            }
            else
            {
                winnerID.Value = 0;
                Debug.LogError("Draw");
            }
            timerStarted.Value = false;
        }
    }
}
